---
title: Docker Drag and Drop
date: 2018-01-13
---
Once Docker had gained steam within the corporate world I had been asked a couple of times "what it would take to dockerize our application?" At the time I didn't really know anything about docker but since then I have figured a few things out. :) 

I want to share what I've found in taking a pre-existing application and "dockerizing" it. It's not taking a final product and giving you another final product, rather this is more of an exploratory post about the options available in hopes that it helps somebody out there. With that said I'll take a generic PHP application that just uploads an image and lists what's stored in the database in order to figure out the media storage, database storage, docker-compose.yml file, etc.... This will simulate what I would need to take a currently running application on VMs and moving it over to Docker. 

We'll tackle it in sections

- Nginx, PHP with Codeigniter and Unirest 
- Images/Media storage with [SeaweedFS](https://github.com/chrislusf/seaweedfs)
- Database storage with [StorageOS](https://storageos.com/) and MariaDB

What you'll need to replicate this are 3 VMs that can access the internet...ya know...to download stuff :)

For this I'm going to cheat a little and use a pre-existing StorageOS install on Ubuntu 16.04 using [VirtualBox](https://www.virtualbox.org/wiki/Downloads) with [Vagrant](https://www.vagrantup.com/). You can download the Vagrant file [here](https://docs.storageos.com/assets/Vagrantfile) from the good people at StorageOS.

Once you have downloaded the Vagrant file just type to following to get it up and running. Note: if you are getting timeout messages you can set `config.vm.boot_timeout = 1000`

```
# directory where you downloaded the Vagrantfile to
vagrant up

```

I want to note that this example doesn't consider every single thing you'll need. There are reverse proxies, db optimizations, php settings, seaweedfs settings to allow for certain sizes, backups, etc...; we are just going to focus on "can this even be done". Hopefully I have shown you something that you can take a few examples here to help implement whatever you are building.

# NGINX and PHP

So when I was looking to convert my PHP application (running Codeigniter 3) to run on Docker I looked for the PHP docker container as well we an nginx container and then tried to figure out how to wire them up. It was then that I thought "surely someone has already done this and created a Docker image" and to my relief there was. [richarvey/nginx-php-fpm](https://hub.docker.com/r/richarvey/nginx-php-fpm/) had everything that I needed for my PHP application to get up and running. If you look at the wonderful docs on the Docker page it has things listed for git, [letsEncrypt](https://letsencrypt.org/) as well as xDebug. Now I don't think you'll need everything that that image comes with, I know I didn't, but you can remove what you don't need by building your own image. For the sake of this article I'm just going to leave it alone.

Here is an example of the Dockerfile I created for my PHP app.

```
FROM richarvey/nginx-php-fpm
EXPOSE 80
EXPOSE 443
ENV WEBROOT=/var/www/html/public
ENV MARIADB_DATABASE=webapp
ENV MARIADB_USERNAME=codeigniter
ENV MARIADB_PASSWORD=password
ENV MARIADB_HOST=mariadb
ENV BASE_URL=http://local.dev/
COPY ./ /var/www/html/
COPY ./nginx.conf /etc/nginx/sites-available/default.conf
```
Here I copy over my Codeigniter code, that uses the Unirest PHP package to upload images, to the `/var/www/html` directory as well as a nginx config file so when I build it it will have an nginx.conf file dedicated to my Codeigniter setup. I also set the webroot to `/var/www/html/public` since I moved the index.php file in there. Note: if you do this you'll need to update the index.php file to get the proper path to the `application` and `system` folders. Here is what my folder structure and nginx.conf file looks like.

```
-rw-r--r--@  1 michael  staff   296B Jan 12 21:44 Dockerfile
drwxr-xr-x@ 19 michael  staff   608B Jan 11 16:58 application
-rw-r--r--@  1 michael  staff   2.0K Jan 11 16:02 nginx.conf
drwxr-xr-x@  4 michael  staff   128B Jan 11 16:52 public
drwxr-xr-x@ 10 michael  staff   320B Jan 11 13:06 system
```

```
server {
        listen   80; ## listen for ipv4; this line is default and implied
        listen   [::]:80 default ipv6only=on; ## listen for ipv6

        root /var/www/html/public/;
        index index.php;

        # Make site accessible from http://localhost/
        server_name _;

        # Disable sendfile as per https://docs.vagrantup.com/v2/synced-folders/virtualbox.html
        sendfile off;


        error_log /dev/stdout info;
        access_log /dev/stdout;

        location / {
                try_files $uri $uri/ /index.php;
        }

        error_page 404 /404.html;

        location = /404.html {
                root /var/www/errors;
                internal;
        }

        location ^~ /ngd-style.css {
            alias /var/www/errors/style.css;
            access_log off;
        }

        location ^~ /ngd-sad.svg {
            alias /var/www/errors/sad.svg;
            access_log off;
        }

        # pass the PHP scripts to FastCGI server listening on socket
        location ~ \.php$ {
                try_files $uri =404;
                fastcgi_split_path_info ^(.+\.php)(/.+)$;
                fastcgi_buffers 16 16k;
                fastcgi_buffer_size 32k;
                fastcgi_pass unix:/var/run/php-fpm.sock;
                fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
                fastcgi_param SCRIPT_NAME $fastcgi_script_name;
                fastcgi_index index.php;
                include fastcgi_params;
        }

        location ~* \.(jpg|jpeg|gif|png|css|js|ico|xml)$ {
                expires           5d;
        }

        # deny access to . files, for security
        location ~ /\. {
                log_not_found off;
                deny all;
        }

        location ^~ /.well-known {
                allow all;
                auth_basic off;
        }

}
```

I also updated my `/etc/hosts` file to set local.dev to point to 127.0.0.1. so when I run 'docker-compose up' (locally, not in the docker swarm yet) I can use the proper BASE_URL I set in the environment variables. I also added an entry to point web1.dev to 192.168.50.100 for the virtualbox VMs that I'll be using for this example. When you would deploy this using the docker stack command, the BASE_URL would be set to http://web1.dev/ so that Codeigniter wont use the internal Docker swarm ip address that isn't accessbile from the outside.

```
127.0.0.1       local.dev
192.168.50.100  web1.dev
```
## Switching the OS?
Now I want to note that most of my applications that are in production run on Ubuntu, but in this case it will be running on Apline Linux. Why the switch? Well when it comes to deployment Alpine is smaller than Ubuntu; it doesn't have a lot of things pre-installed making it a small linux distro, so that in turn makes it a lighter OS for deployments. That being said richarvey/nginx-php-fpm docker image was perfect since it was built on top of this. I'll admit that finding this specific image was a God send because the options out there are pretty daunting, there are so many images to choose from. So my advice is to find something close to what you need (if you are not going to build it yourself) and take out what you don't need. I looked at the number or 'Pulls' and the frequency of it's updates to make sure I'm not getting some stale image.

In order for my example Codeigniter app to use the environment variables defined in the Dockerfile (or the docker-compose.yml file...down below) I needed to update my code to get these settings by calling the PHP method `getenv()`. In my `application/config/database.php` file of the example app, I used the getenv function to get what I set in the Dockerfile.

```php
$db['default'] = array(
        'dsn'   => '',
        'hostname' => getenv('MARIADB_HOST'),
        'username' => getenv('MARIADB_USERNAME'),
        'password' => getenv('MARIADB_PASSWORD'),
        'database' => getenv('MARIADB_DATABASE'),
        'dbdriver' => 'mysqli',
        'dbprefix' => '',
        'pconnect' => FALSE,
        'db_debug' => (ENVIRONMENT !== 'production'),
        'cache_on' => FALSE,
        'cachedir' => '',
        'char_set' => 'utf8',
        'dbcollat' => 'utf8_general_ci',
        'swap_pre' => '',
        'encrypt' => FALSE,
        'compress' => FALSE,
        'stricton' => FALSE,
        'failover' => array(),
        'save_queries' => TRUE
);
```

So my webapp part of things is complete. This would be similar if you were making a microservice in PHP using something like slimPHP or Lumen. You'd just need to have the proper nginx.conf file and the proper environment variables set. For this little example it's all I needed. I was read to build my web application image to be used in Docker Swarm. I built it using the command below and pushed it up to my repo.

```
docker build -t trackleaf/example-webapp:0.0.3 .

docker push trackleaf/example-webapp:0.0.3

The push refers to repository [docker.io/trackleaf/example-webapp]
25b4b5aea8d1: Pushed
e12792647c7b: Pushed
60ed0d55ed17: Layer already exists
6a2f02997f22: Layer already exists
9a1f6bec0570: Layer already exists
e9d191f84574: Layer already exists
afb8edfdf8dc: Layer already exists
651ce92f38c9: Layer already exists
74463e2b2264: Layer already exists
e96340297de0: Layer already exists
3b8d2fea8c51: Layer already exists
df136daf5990: Layer already exists
9614caa01667: Layer already exists
e16493b8b32d: Layer already exists
4e5091ee8b2c: Layer already exists
54d04b25eee4: Layer already exists
cbda1dd714bb: Layer already exists
7244e6630d42: Layer already exists
8892a35d2da5: Layer already exists
d5b42f8698f9: Layer already exists
2fbbd5860aac: Layer already exists
1c74dd9b4cb3: Layer already exists
46a4e17e25bb: Layer already exists
bbb04ef95459: Layer already exists
e4472ff5cf6a: Layer already exists
e77c9bf7a0ae: Layer already exists
c6b3b54fc84f: Layer already exists
be8889419a99: Layer already exists
b8ba8e18a152: Layer already exists
d17a2ac00469: Layer already exists
52a5560f4ca0: Layer already exists
0.0.3: digest: sha256:0d11aff53d06aee0b2ede52279929519eec138fe5a8d0bd6d7bc69e93a8eac3f size: 6779

```

As you can see from the output above that I have built and pushed the example-webapp a few times and Docker only updated the layers that changed. 

I have this build ready to go if you want to pull it down at [https://hub.docker.com/r/trackleaf/example-webapp/](https://hub.docker.com/r/trackleaf/example-webapp/)

# Images with SeaweedFS

In my example application I've given it the ability to upload an image to an image service running on Docker, this mimics something that I did in production. I created a service in PHP that accepted images and stored them away; but there was no replication as well as other features SeaweedFS has out of the box. So in the example app if you go to /upload it brings you to an upload form (it's pretty much the CodeIgniter example for the docs) that uploads the images to the  service I have running. 

What I liked about this service is that instead of me coming up with a small PHP micro-service that essentially takes images, stores them and serves them I could use this service, that is built on the Go programming language, that does this for me and also has the ability to replicate everything. There is an introduction and list of features [here](https://github.com/chrislusf/seaweedfs#introduction). I found this library after not wanting to figure out the whole replication thing for the small micro-service I already had running.

Now to get this dockerized I created my own image of it using this Dockerfile

```
FROM frolvlad/alpine-glibc:alpine-3.5

RUN apk add --no-cache --virtual build-dependencies --update wget curl ca-certificates && \
    wget -P /tmp https://github.com/$(curl -s -L https://github.com/chrislusf/seaweedfs/releases/latest | egrep -o 'chrislusf/seaweedfs/releases/download/.*/linux_amd64.tar.gz') && \
    tar -C /usr/bin/ -xzvf /tmp/linux_amd64.tar.gz && \
    apk del build-dependencies && \
    rm -rf /tmp/*

EXPOSE 8080
EXPOSE 9333

VOLUME /data
```
When I run it, I add the command params to it along the lines of:

```
# this was taken from the docker-compose.yml file below
command: 'volume -max=5 -mserver="seaweed-master:9333" -port=8080'
```

When it comes to each node that runs SeaweedFS this is how I prepared the directories

## SeaweedFS Directories
```
# master server
sudo mkdir /seaweedm

# volume servers
sudo mkdir /seaweedv
```

# MariaDB and StorageOS

Currently Docker does not have anything that will scale across nodes connected by Docker Swarm when it comes to the database files. So I looked at 2 different solutions, external storage and a Docker volume plugin called StorageOS. When I mean external storage I mean something like having your database hosted on an external service like Amazon, Google, or some VM you have a DB installed on. Even though this is in no way utilizing Docker there is no shame in it. Honestly if you are comfortable where you are with your DB setup there is no shame in leaving it alone and just white-listing the nodes that the containers reside on to get DB access. For every person I talk to, blog I read, book I buy there is always a different solution when it comes to deployments.

Now for those looking to have Docker containers persist the data and not worry about it going away once the container is blown away or the database being restricted to a specific node in the swarm, we can use a Docker volume plugin called StorageOS. For those that won't be using the vagrant file, which takes care of the StorageOS setup below, you can use the commands I have listed here to get StorageOS up and running, it's basically the commands the Vagrant file has in it.

SSH into each of the VMs. The following commands have been broken into sections for easier reference :)

## NBD
```
# configure network block device
# Add the following line to /etc/modules to ensure NBD is loaded on reboot
nbd

# Add the following module configuration lines in /etc/modprobe.d/nbd.conf
options nbd nbds_max=1024
options nbd max_part=15

# set it for the current session since the settings above will take effect on reboot
sudo modprobe nbd nbds_max=1024
```

## Ports 
```
# UFW
# open up the ports needed by StorageOS

sudo ufw default allow outgoing
sudo ufw allow 5701:5711/tcp
sudo ufw allow 5711/udp

# iptables version
# same as above but for iptables instead of ufw

sudo iptables -I INPUT -i lo -j ACCEPT
sudo iptables -I OUTPUT -o lo -j ACCEPT
sudo iptables -I OUTPUT -o eth0 -d 0.0.0.0/0 -j ACCEPT
sudo iptables -I INPUT -i eth0 -m state --state ESTABLISHED,RELATED -j ACCEPT
sudo iptables -A INPUT -p tcp --dport 5701:5711 -j ACCEPT
sudo iptables -A INPUT -p udp --dport 5711 -j ACCEPT
```

## StorageOS container
```
# Run the StorageOS container
docker run -d --restart unless-stopped --name storageos \
    -e HOSTNAME \
    -e ADVERTISE_IP={CURRENT_NODE_IP_ADDRESS} \
    -e JOIN={NODE_1_IP_ADDRESS},{NODE_2_IP_ADDRESS},{NODE_3_IP_ADDRESS} \
    --net=host \
    --pid=host \
    --privileged \
    --cap-add SYS_ADMIN \
    --device /dev/fuse \
    -v /var/lib/storageos:/var/lib/storageos:rshared \
    -v /run/docker/plugins:/run/docker/plugins \
    storageos/node:0.9.2 server
```

## StorageOS CLI
```
# Install the StorageOS CLI
curl -sSL https://github.com/storageos/go-cli/releases/download/0.9.1/storageos_linux_amd64 > /usr/local/bin/storageos

chmod +x /usr/local/bin/storageos
```

## Set ENV Variables
```
echo "export STORAGEOS_USERNAME=storageos STORAGEOS_PASSWORD=storageos STORAGEOS_HOST={NODE_1_IP_ADDRESS} ADVERTISE_IP={CURRENT_NODE_IP_ADDRESS}" > /home/{user}/.profile

echo "Defaults env_keep += \\"STORAGEOS_HOST STORAGEOS_USERNAME STORAGEOS_PASSWORD ADVERTISE_IP\\"" > /etc/sudoers.d/00_storageos_env
```

That should have StorageOS good to go on your VMs.

Now that we have our directories created for SeaweedFS and everything set up for StorageOS let's get Docker Swarm up and running. We'll be following StorageOS' setup, you can read the docs [here](https://docs.storageos.com/) for more information as to why.

```
# on node1 (storageos-1..if you are using the vagrant file)
docker swarm init --advertise-addr 192.168.50.100

# we'll create the swarm with more managers for StorageOS
# log into the other nodes and copy and past the output of the following command 
docker swarm join-token manage
```
Once we have the swarm up and running you can use the following docker-compose.yml file to wire everything up

# Docker Swarm
```
docker stack deploy --compose-file=docker-compose.yml example-app

# You should see the something like the following output

vagrant@storageos-1:~$ docker service ls
ID                  NAME                         MODE                REPLICAS            IMAGE                              PORTS
u8kdml0vtpce        example-app_adminer          replicated          1/1                 adminer:latest                     *:8081->8080/tcp
nu9kaqi8akfr        example-app_mariadb          replicated          0/1                 trackleaf/example-database:0.0.1   *:3306->3306/tcp
nv9ebex9n61b        example-app_seaweed-master   replicated          1/1                 trackleaf/seaweedfs:0.0.1          *:9333->9333/tcp
gqvb6eujqr32        example-app_seaweed-volume   replicated          1/1                 trackleaf/seaweedfs:0.0.1          *:8080->8080/tcp
5t9j1kbzwbca        example-app_webapp           replicated          0/1                 trackleaf/example-webapp:0.0.3     *:80->80/tcp

```

# docker-compose.yml file for the stack
```
version: '3'
services:
  webapp:
    image: 'trackleaf/example-webapp:0.0.3'
    ports:
      - '80:80'
    depends_on:
      - mariadb
    environment:
      - BASE_URL=http://web1.dev/
  mariadb:
    image: 'trackleaf/example-database:0.0.1'
    deploy:
      replicas: 1
    volumes:
      - db:/var/lib/mysql
    ports:
      - '3306:3306'
    command: --ignore-db-dir=lost+found
  adminer:
    image: adminer
    restart: always
    ports:
      - '8081:8080'
  seaweed-master:
    image: 'trackleaf/seaweedfs:0.0.1'
    ports:
      - '9333:9333'
    command: "/usr/bin/weed master -mdir='/data' -ip='192.168.50.100'"
    volumes:
      - /seaweedm:/data
    deploy:
      placement:
        constraints:
          - node.hostname == storageos-1
    networks:
      default:
        aliases:
          - seaweed_master
  seaweed-volume:
    image: 'trackleaf/seaweedfs:0.0.1'
    ports:
      - '8080:8080'
    command: '/usr/bin/weed volume -max=5 -mserver="seaweed-master:9333" -ip="192.168.50.101" -port=8080'
    volumes:
      - /seaweedv:/data
    depends_on:
      - seaweed-master
    deploy:
      placement:
        constraints:
        - node.hostname == storageos-2
    networks:
      default:
        aliases:
          - seaweed_volume

volumes:
    db:
      driver: storageos
```
If you've updated your `/etc/hosts` file you should be able to access the application at http://web1.dev/upload

You should also be able to upload images and have everything working as it should.

So with that docker-compose.yml file deploying our stack we have everything that the PHP web application needs. We have our Database, image server, environment variables to take this application and "dockerize" it. This is by no means exhaustive when it comes to optimizations or anything like that, it's more the proof-of-concept that you and I can use to build on. There is so much more to Docker, StorageOS and SeaweedFS that we can do. Below are some links to get you started on these technologies.

- [SeaweedFS](https://github.com/chrislusf/seaweedfs/wiki)
- [Codeigniter](https://www.codeigniter.com/user_guide/)
- [StorageOS](https://docs.storageos.com/)

