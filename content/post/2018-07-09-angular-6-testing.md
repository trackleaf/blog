---
title: Testing with Angular 6
date: 2018-07-09
---

I'm going to be honest, I'm a "addition by subtraction" type of guy; meaning I don't like extra baggage when doing development because it always leads to a headache down the road. The headache when I started doing Angular 6 development after doing Polymer (love it!) and React (meh) development, wasn't so much learning typescript (my goodness, why the layer on top of javascript) it was testing components in Angular.

The problem: Testbed

The Solution: Get rid of Testbed

When you use tha angular-cli to create a new component it typically generates a few files with it, the one in particular we are dealing with is the test file. In this test file you'll see it creates the component and tests if it exists...the default test. All well and good until you have to start including dependencies. I have found that the amount of setup code to include a dependency was ridiculous. Providers, imports etc...it was just to much so the way I went
about testing was removing everything except the import of the component itself and did the whole

`const component = new MyComnponent();`

If there was anything that needed to be injected into the component all I did was create a mock object and inserted it into the component like so:

```
const mockLocation = {
    navigateByUrl: jasmine.createSpy()
};

const component = new MyComponent(<any> mockLocation);

// then in the test itself....

component.someFunction();

expect(mockLocation.navigateByUrl).toHaveBeenCalled(); // true if someFunction makes a call to this.location.navigateByUrl in it

```

boom.

simple....

the way I like it. 

All I had to worry about now was that any injected dependency had a mock for any method that is call within my component.

To further simplify my testing I moved logic out of promises and subscribers. Example:

```
// Before

someMethod() {
    this.http.get('/an/api/path').subscribe((results) => {
        if(result.success === true) {
            // do something interesting
        } else {
            // do something else even more interesting
        }
    });
}

// After
someMethod() {
    this.http.get('/an/api/path').subscribe(this.handleAPIResponse.bind(this)); // bind to this so that we have that reference in the method
}

handleAPIResponse(result) {
    if(result.success === true) {
        // do something interesting
    } else {
        // do something else even more interesting
    }
}
```
So now in my tests I can directly test that someMethod called http.get (which we can easily mock) AND THEN we test that the logic performs correctly in handleAPIResponse method by testing it directly.

The other thing I figured out when testing with Angular is mocking window properties. I do that by doing this

```
 spyOnProperty(window, 'innerWidth').and.returnValue(1);
```

So in conclusion doing these few things has greatly simplified my tests and made me a happier developer when using Angular.
