---
title: Hapi + JWT + Knex + WebSockets + Nes
date: 2018-01-23
---
After working in the PHP area for quite a while I wanted to see if I could build a RESTful API in another language, and in this case I was going to do it in JS. After looking at a couple of JS frameworks I decided on HapiJS. It was quick and painless to set up. The only problem I had as of this writing was that a new version of Hapi was released but the examples on the website weren't updated, so I had to read the API; minor inconvinence but I usually end up reading the API docs anyway. :)

I based my folder structure on the Codeigniter structure since I was familiar with that, so if in case you are asking yourself why I did it that way.

Now these were the things I included in my little exploritory app:

- [Hapi](https://hapijs.com/) - [API](https://github.com/hapijs/hapi/blob/master/API.md)
- [Knex](http://knexjs.org/)
- [JWT](https://jwt.io/)
- [Nes](https://github.com/hapijs/nes)
- Docker

You can clone the repo [here](https://gitlab.com/trackleaf/hapi-jest-knex-jwt/tree/master)

Install all the dependencies
```
yarn install
```

To launch the example application use this command

```
JWT_PRIVATE_KEY=supersecret MYSQL_USER=hapijs MYSQL_PASSWORD=pass MYSQL_DATABASE=hapijs MYSQL_PORT=3307 node server.js
```

If you notice I'm setting an env variable so that when I read in the private key in the code I'm using this to grab it: `const privateKey = process.env.JWT_PRIVATE_KEY;`

## Database
Start a mariadb docker container and create the database as well as grant access to our hapijs user. Notice that in this instance I set the port mapping to something different since currently I'm using 3306 locally :)
```
>docker run --name some-mariadb -e MYSQL_ROOT_PASSWORD=my-secret-pw -e MYSQL_USER=hapijs -e MYSQL_PASSWORD=pass -d -p '3307:3306' mariadb:latest
>docker exec -it some-mariadb '/bin/bash'
>mysql -u root -p'my-secret-pw'

CREATE DATABASE hapijs;

GRANT ALL PRIVILEGES ON hapijs . * TO 'hapijs'@'%';
```

Now that I had my database up and running I needed to create the table and populate it with a little bit of data.
```
MYSQL_USER=hapijs MYSQL_PASSWORD=pass MYSQL_DATABASE=hapijs MYSQL_PORT=3307 knex migrate:latest

MYSQL_USER=hapijs MYSQL_PASSWORD=pass MYSQL_DATABASE=hapijs MYSQL_PORT=3307 knex seed:run
```

Now I'll say this, even though Knex as well as other npm packages out there provide a good way to building tables and populating it, I'd rather stick to good ole `.sql` files and create/populate the database that way. I've seen way to many development accidents to like migrations. That being said, I provided a simple version here for those that want that sort of thing.

## Authentication / JWT
With everything up and running I used [Postman](https://www.getpostman.com/) to test my API. 

- First I logged into the API to get my JWT token to be able to access the restricted routes by posting email and password to the `sessions/create` route

![Create Session](/img/session-create.png)

- Second I used that token to get a restricted route by adding a header key/value

![Restricted Path](/img/restricted-path.png)

## Tests
For those that can't live without tests I've included a testing library that I really like called [Jest](https://facebook.github.io/jest/). The reason why I like it is because there is next to zero setup. I don't know how many projects I've been apart of that had a long config file to get tests all wired up. Here with jest I just reference the file I'm testing and add in some mocks where I need it and I'm off to the races. Here is what I have in my simple handlers test.

```javascript
const handler = require('../../application/handlers/admin').admin;
 // dummy test :)
jest.mock('../../application/models/users');
const users_model = require('../../application/models/users');

test('users method', function () {
        // setup
        users_model.getAdminUsers = jest.fn().mockReturnValueOnce([1,2,3]);
        users_model.loadDb = jest.fn();
        const requestMock = {
                server: {
                        app: {
                                db: jest.fn()
                        }
                }
        };

        // call
        const result = handler.users(requestMock, {});

        // test
        expect(users_model.loadDb.mock.calls.length).toBe(1);
        expect(result.length).toBe(3);
});
```

## Web Sockets
This API doubles as a web socket backend in which we use [nes](https://github.com/hapijs/nes) that utilizes Hapi routes, It's also maintained by the person that maintains HapiJS. Check out the nes docs for more info on what you can do with that package. I included the client.js file as an example of testing the web socket connection. This was taken from the nes example on the github page.

```javascript
// application/routes/ws.js
const handlers = require('../handlers/base');

module.exports = [
    {
            method: 'GET',
            path: '/us',
            config: {
                id: 'user-status',
                handler: function (request, h) {
                        return 'user updated profile';
                }
            }
    }
];


// client.js
const Nes = require('nes');

var client = new Nes.Client('ws://localhost:3000');

const start = async () => {

    await client.connect();
    const payload = await client.request('user-status');  // Can also request '/us'
    console.log(payload);
};

start();
```

## Conclusion
Honestly this is just a start for a hapi project, the sky is the limit. Hopefully this helps someone out there that's getting started with Hapi like I am. One thing that I'm not sure I'm happy with is how the database is loaded; it could be a little more automatic, but I'm cool with this for now.