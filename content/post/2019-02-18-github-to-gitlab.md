---
title: Going from Github to Gitlab 
date: 2019-02-18
---
### The Setup

So I'm doing a front-end/micro-api project based on javascript. It's ExpressJS serving up a single page application along with having a few /api routes that essentailly act as a proxy for other backend services out of my development control. 

Luckly the delivery/deployment process is simply providing a [Docker](https://docs.docker.com/) image for some other part of the company to consume and this is where Gitlab comes in.

I don't really go in depth on the different things Gitlab offers but I'm adding links in this article to the [docs](https://docs.gitlab.com/ee/README.html) that should give you more info on all the things Gitlab offers.

### Enter Gitlab

Outside of providing a code repo with a built in wiki, issue tracker and code snippets (pretty standard) it also comes with it's own CI/CD as well as private docker registry.

Here is what I did to take the Node/SPA application and create a Docker image that is ready for whomever to consume.

Here is my project layout:

```
-rw-r--r--     1 user  staff    13B Feb 12 15:40 .dockerignore
-rw-r--r--     1 user  staff   146B Feb 15 08:49 .editorconfig
-rw-r--r--     1 user  staff   466B Feb 15 08:49 .eslintrc.json
drwxr-xr-x    15 user  staff   480B Feb 18 11:18 .git
-rwxr-xr-x     1 user  staff   310B Feb 12 15:40 .gitignore
-rw-r--r--     1 user  staff   578B Feb 15 08:49 .gitlab-ci.yml
drwxr-xr-x     4 user  staff   128B Feb 12 15:40 .storybook
-rw-r--r--     1 user  staff   496B Feb 15 09:00 Dockerfile
-rwxr-xr-x     1 user  staff   5.4K Feb 15 10:57 README.md
drwxr-xr-x     9 user  staff   288B Feb 12 15:40 build
drwxr-xr-x  1182 user  staff    37K Feb 13 17:07 node_modules
-rw-r--r--     1 user  staff   1.3K Feb 15 10:13 package.json
drwxr-xr-x     5 user  staff   160B Feb 12 15:40 public
drwxr-xr-x     4 user  staff   128B Feb 12 15:40 server
-rw-r--r--     1 user  staff   1.1K Feb 15 08:49 server.js
drwxr-xr-x    14 user  staff   448B Feb 15 16:59 src
-rw-r--r--     1 user  staff   512K Feb 15 08:49 yarn.lock
```

This is a base [React-Scripts](https://facebook.github.io/create-react-app/) project with a few things thrown in there to handle the `/api` portion. There are 2 key files that we need to pay attention to and that is `Dockerfile` and `.gitlab-ci.yml`.

The Dockerfile has this:

```
FROM node:8-alpine

COPY . /app

WORKDIR /app

RUN yarn
RUN yarn build

EXPOSE 80
CMD yarn server
```
What we are doing here is:

* getting the node JS (version 8) image, based on the Alpine operating system
* copying over the code
* running `yarn` (this is like: npm install) and `yarn build` (_Note: yarn comes pre-installed on all NodeJS images_)
* exposing the nodeJS server on port 80 when a container based on this image is running.

The Gitlab CI file has this:

```
stages:
- test
- build

test_cases:
 stage: test
 image: node:latest
 script:
   - yarn
   - yarn test-ci
   - yarn test-api

build_docker_image:
 stage: build
 image: docker:git
 services:
   - docker:dind
 script:
   - docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN registry.gitlab.com
   - docker build -t registry.gitlab.com/project/name:$CI_COMMIT_SHA -t registry.gitlab.com/project/name:latest .
   - docker push registry.gitlab.com/project/name:$CI_COMMIT_SHA
   - docker push registry.gitlab.com/project/name:latest
 only:
   - master
```

Now let me explain what this does and how it interacts with the Docker file.

This gitlab CI file ([.gitlab-ci.yml](https://docs.gitlab.com/ee/ci/yaml/)) has [stages](https://docs.gitlab.com/ee/ci/yaml/#stages) listed with `test` and `build`.

For the `test` stage we label it as `test_cases` and what it does is it uses the latest node docker image to run `yarn`, `yarn test-ci` and `yarn test-api` commands against the current code base.

Here are the `test-ci` and `test-api` commands located in the package.json file if you are curious:

```
{
    “test-api”: “jest --verbose ./server”,
    “test-ci”: “CI=true react-scripts test”
}
```
One command to test the API endpoints provided by our server code and the other to test the react components.

When Gitlab runs these tests, based on the commands listed in the `script` section, if there are any failures in the tests it will block the next thing in the pipeline, which is the `build` stage, from happening.

In the `build` stage we use a docker image `docker:git` to log into the gitlab registry and then issue a docker build command and tag it with 2 different tages, one for the latest and the other versioned with the git commit sha. The great thing is is that we can restrict this to a specific branch which if you look at the [only](https://docs.gitlab.com/ee/ci/yaml/#only-and-except-simplified) section we defined it as `master`, so this will only run when we update the code in master via push or merge from any other branch.

Once the image is build we push it to our private registry to where anyone with the proper access credentials can pull from it.

We have eliminated the need for any other 3rd party tooling since we are now just using Gitlab to do our testing as well as building. Simple. The way I like it.

There is so much **more** that can be done for testing, building and deploying so here is a link to the documentation: [https://docs.gitlab.com/ee/ci/](https://docs.gitlab.com/ee/ci/)
